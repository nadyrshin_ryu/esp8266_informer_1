//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// ����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "esp_common.h"
#include <freertos/FreeRTOS.h>
#include <freertos/FreeRTOSConfig.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include "../driver/gpio.h"
#include "buttontask.h"


static uint8_t task_stopping, task_run;
xQueueHandle qButton;


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void button_Init(void)
{
	//PIN_FUNC_SELECT(GPIO_PIN_REG(BUTTON_PinNum), FUNC_GPIO0);

	uint8_t ButtonState = 1, ButtonStateOld = 1;
	GPIO_ConfigTypeDef gpio_struct;
	gpio_struct.GPIO_Pin = (1 << BUTTON_PinNum);
	gpio_struct.GPIO_Mode = GPIO_Mode_Input;
	gpio_struct.GPIO_Pullup = GPIO_PullUp_EN;
	gpio_struct.GPIO_IntrType = GPIO_PIN_INTR_DISABLE;
	gpio_config(&gpio_struct);
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void buttontask(void *pvParameters)
{
	// ���������� ��������� �� ������� ��
	xQueueHandle *queue = (xQueueHandle *) pvParameters;
	if (queue == NULL)
	{
		vTaskDelete(NULL);
		return;
	}

	uint8_t ButtonState = 1, ButtonStateOld = 1;
	uint32_t ButtonPressed = 0;
	uint8_t ButtonPressCode;
	uint8_t Sended = 0;

	while (!task_stopping)
	{
		ButtonState = GPIO_INPUT_GET(BUTTON_PinNum);
		if ((!ButtonState) && ButtonStateOld) 			// ��������� ����� - ������ ������
		{
			ButtonPressed = 0;
			Sended = 0;
		}
		else if ((!ButtonState) && (!ButtonStateOld))	// ������ ������������ �������
		{
			ButtonPressed++;

			if ((ButtonPressed >= BUTTON_LongPress) && (!Sended))	// ������� �������
			{
				ButtonPressCode = BUTTON_Code_LongPress;
				// ���������� � ������� ��� ���� ������� �� ������
				xQueueSendToBack(queue, &ButtonPressCode, 0);
				Sended = 1;
			}
		}
		else if (ButtonState && (!ButtonStateOld))		// ����������� ����� - ������ ������
		{
			if (ButtonPressed < BUTTON_LongPress)		// �������� �������
			{
				ButtonPressCode = BUTTON_Code_ShortPress;
				// ���������� � ������� ��� ���� ������� �� ������
				xQueueSendToBack(queue, &ButtonPressCode, 0);
			}
		}
		ButtonStateOld = ButtonState;

		vTaskDelay(10);
	}

	task_run = 0;
	// ����������� ������, ������� ��� ������� ��������� � ��������
	vQueueDelete(queue);
	// ��������� ��������� ������� ������
	vTaskDelete(NULL);
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ��������� ������� ������ ������ ������
//------------------------------------------------------------------------------
void buttontask_start(void)
{
	task_stopping = 0;
	button_Init();

	qButton = xQueueCreate(10, sizeof(uint8_t));
	xTaskCreate(buttontask, "ButtonTask", 256/*128*/, qButton, 3, NULL);

	task_run = 1;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ��������� ��������� ������ ������ ������. ��������� ��������� ������� �����
// �� ������� ���������� ������.
//------------------------------------------------------------------------------
void buttontask_stop(void)
{
	task_stopping = 1;
	while (task_run)
	{
		vTaskDelay(1);
	}
}
//------------------------------------------------------------------------------
