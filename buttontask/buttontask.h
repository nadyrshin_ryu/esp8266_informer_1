//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// ����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef BUTTONTASK_BUTTONTASK_H_
#define BUTTONTASK_BUTTONTASK_H_


// ���, � �������� ���������� ������
#define BUTTON_PinNum	0
#define BUTTON_Pin		(1 << BUTTON_PinNum)

// ���� ������� �������
#define BUTTON_Code_ShortPress	1	// �������� �������
#define BUTTON_Code_LongPress	2	// ������� �������

// ���������� ����� ������, ������� � �������� ������� ��������� �������
#define BUTTON_LongPress		20


// ������� ��, ����� ������� ���������� ���� �������
extern xQueueHandle qButton;


// ��������� ������� ������ ������ ������
void buttontask_start(void);
// ��������� ��������� ������ ������ ������. ��������� ��������� ������� ����� �� ������� ���������� ������.
void buttontask_stop(void);


#endif /* BUTTONTASK_BUTTONTASK_H_ */
