//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// ����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef MESSAGETASK_MESSAGETASK_H_
#define MESSAGETASK_MESSAGETASK_H_

#include <stdint.h>

#define MESSAGES_INPUT_QUEUE_SIZE	16		// ������ ������� �� ��������� (��� �� ������, ��� ������ ��������� ����� �������� ��������� �� ��� (������ ��� �� �� �������)
#define MESSAGES_POOL_SIZE			64		// ������ ������ ���������
#define MESSAGES_SHOW_TICK			10		// ���-�� ����� �� (10��) �� ���� ��� ����������� ���������

#define MESSAGES_MAX_PRIORITY		127		// ������������ ��������� ���������


// �������� ���������, � ������� ������� ����� ��������� ���������� ������-������������� ���������
typedef struct
{
	uint8_t *pStr;
	uint8_t AnimationIn;
	uint8_t AnimationOut;
	uint16_t TickNormal;
	uint16_t TickBraked;
	uint16_t ShowTime;
	uint8_t HAlign;
	int8_t Priority;
	uint8_t Font;
	uint8_t IsDynamicImage;
	void *Mutex;
} sqMessage;



// ��������� ������� ������ ������������� ���������
void messagetask_start(void);
// ��������� ��������� ������ ������������� ���������. ��������� ��������� ������� ����� �� ������� ���������� ������.
void messagetask_stop(void);
// ������� ���������� ��������� msg � ������. ������� �� ������ ���������� �� ������������ ����������.
int8_t message_Add(sqMessage *Message);
// ������� ���������� ��������� msg � ������. ������� ������ ��� ������ �� ������������ ����������. ������������ ������ ����������� ���������.
int8_t message_AddFromISR(sqMessage *Message);
// ��������� ������� ������ ���������
void message_Clear(int8_t MaxPriority);


#endif /* MESSAGETASK_MESSAGETASK_H_ */
