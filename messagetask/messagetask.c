//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// ����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "messagetask.h"
#include <freertos/FreeRTOS.h>
#include <freertos/FreeRTOSConfig.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include <freertos/semphr.h>
#include <animations.h>


// ���� ��������� ���������
#define MessageState_Free	0
#define MessageState_Idle	1
#define MessageState_Run	2


// ��������� �������� ��������� � ������
typedef struct
{
	int8_t *pStr;
	uint8_t AnimationIn;
	uint8_t AnimationOut;
	uint16_t TickNormal;
	uint16_t TickBraked;
	uint16_t TimeLeft;
	uint16_t TimeFull;
	int8_t Priority;
	uint8_t HAlign;
	uint8_t State;
	uint8_t Font;
	uint8_t IsDynamicImage;
	xQueueHandle Mutex;
} sMessage;

// ���� ����, ��� ������ ��������
static uint8_t task_run = 0;
// ���� ����, ��� �������� ������ �� ���������� ������ ������
static uint8_t task_stopping = 0;
// ��������� �����
static uint8_t Buff[300];

// ������� FreeRTOS, ����� ������� ��������� ����������� ������ ��������� ���������
static xQueueHandle qMessages = NULL;
// ������� ��� ��������� ����� �������� �������� ������������� � ������������ � �� ������������ � �������������� �����������
static sMessage MessagesPool[MESSAGES_POOL_SIZE];
// ������ ������� � ���� ���������. ��������� ������������� ������ � ���� ��������� �� ���������� �������
static xQueueHandle xSemPool;


//------------------------------------------------------------------------------
// ������� ������ ��������� ��������� � ���������� �����������
//------------------------------------------------------------------------------
static sMessage *GetMaxPriorityIdleMessage(void)
{
	sMessage *message = 0;
	uint8_t MaxPriority = 0;
	int16_t i;

	for (i = 0; i < MESSAGES_POOL_SIZE; i++)
	{
		if ((MessagesPool[i].State == MessageState_Idle) && (MessagesPool[i].Priority > MaxPriority))
		{
			MaxPriority = MessagesPool[i].Priority;
			message = &MessagesPool[i];
		}
	}

	return message;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ������� ������ � ������ ��������� ��������� ������
//------------------------------------------------------------------------------
static sMessage *GetFreeMessage(void)
{
	int16_t i;

	for (i = 0; i < MESSAGES_POOL_SIZE; i++)
	{
		if (MessagesPool[i].State == MessageState_Free)
			return &MessagesPool[i];
	}

	return 0;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ������� ��������� � ������ ��������� ����� ��������� MessageToAdd
//------------------------------------------------------------------------------
static int16_t AddMessageToPool(sqMessage *MessageToAdd)
{
	// ���� � ���� ��������� ��������� ��������
	sMessage *NewMessage = GetFreeMessage();
	if (!NewMessage)
		return -1;

	NewMessage->Mutex = MessageToAdd->IsDynamicImage ? MessageToAdd->Mutex : 0;
	NewMessage->pStr = MessageToAdd->pStr;
	NewMessage->AnimationIn = MessageToAdd->AnimationIn;
	NewMessage->AnimationOut = MessageToAdd->AnimationOut;
	NewMessage->TickNormal = MessageToAdd->TickNormal;
	NewMessage->TickBraked = MessageToAdd->TickBraked;
	NewMessage->Priority = MessageToAdd->Priority;
	NewMessage->TimeFull = MessageToAdd->ShowTime * 10;
	NewMessage->HAlign = MessageToAdd->HAlign;
	NewMessage->State = MessageState_Idle;
	NewMessage->Font = MessageToAdd->Font;
	NewMessage->IsDynamicImage = MessageToAdd->IsDynamicImage;

	printf("Add: %s\n", NewMessage->pStr);
	return 0;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ��������� ������� ��������� Message
//------------------------------------------------------------------------------
static void MessageRun(sMessage *Message)
{
	Message->TimeLeft = Message->TimeFull;
	Message->State = MessageState_Run;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ��������� �������� ��������� �� �������
//------------------------------------------------------------------------------
static void DeleteMessageFromPool(sMessage *MessageToDelete)
{
	// ����������� ������, ������� ��� ��������� �������� ������ ������������ ���������
	if (!(MessageToDelete->IsDynamicImage))
		free(MessageToDelete->pStr);

	MessageToDelete->pStr = 0;
	MessageToDelete->State = MessageState_Free;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ������� ���������� ������������������ ������� �� ��� �������� ���������
//------------------------------------------------------------------------------
static xQueueHandle *GetMessagesQueue(void)
{
	if (qMessages == NULL)
		qMessages = xQueueCreate(MESSAGES_INPUT_QUEUE_SIZE, sizeof(sqMessage));
	return qMessages;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ������� ������� (���������) ��������� �� ������
//------------------------------------------------------------------------------
static int16_t RunMessage(sMessage *Message)
{
	// ����������� ������ � ������
	if ((Message->IsDynamicImage) && (Message->Mutex))
		xSemaphoreTake(Message->Mutex, 0);

	// ����������� ������ � ������ ������ ����� � ��������� � ������ Buff
	int16_t width = ConvertText2TempBuff(Message->pStr, Buff, sizeof(Buff), Message->Font);

	// ����������� ������ � ������
	if ((Message->IsDynamicImage) && (Message->Mutex))
		xSemaphoreGive(Message->Mutex);


	return ani_Run(Buff, width, Message->HAlign);
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ������� ������������� �������� ��������� �� ������
//------------------------------------------------------------------------------
static int16_t RunInMessage(sMessage *Message, int8_t IsBraked)
{
	if ((Message->IsDynamicImage) && (Message->Mutex))
		xSemaphoreTake(Message->Mutex, 0);

	int16_t width = ConvertText2TempBuff(Message->pStr, Buff, sizeof(Buff), Message->Font);

	if ((Message->IsDynamicImage) && (Message->Mutex))
		xSemaphoreGive(Message->Mutex);

	return ani_RunIn(Message->AnimationIn, Buff, width, Message->HAlign, IsBraked ? Message->TickBraked : Message->TickNormal);
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ������� ������������� �������� ������������ � ������
//------------------------------------------------------------------------------
static int16_t RunOutMessage(sMessage *Message, int8_t IsBraked)
{
	if ((Message->IsDynamicImage) && (Message->Mutex))
		xSemaphoreTake(Message->Mutex, 0);

	int16_t width = ConvertText2TempBuff(Message->pStr, Buff, sizeof(Buff), Message->Font);

	if ((Message->IsDynamicImage) && (Message->Mutex))
		xSemaphoreGive(Message->Mutex);

	return ani_RunOut(Message->AnimationOut, Buff, width, Message->HAlign, IsBraked ? Message->TickBraked : Message->TickNormal);
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ������� ���������� ��������� msg � ������. ������� �� ������ ���������� �� 
// ������������ ����������.
//------------------------------------------------------------------------------
int8_t message_Add(sqMessage *msg)
{
	xQueueHandle *queue = GetMessagesQueue();
	if (queue == NULL)
		return -1;

	if (!(msg->IsDynamicImage))
	{
		uint8_t StrLen = strlen(msg->pStr);

		// �������� ������ �� ���� ��� �������� ������ ���������
		uint8_t *str = malloc(StrLen + 1);
		if (!str)
			return -2;	// ���������� ��� ������ ���� ������ �� �������

		// �������� ������ ��������� � ����� ����� ������
		strcpy(str, msg->pStr);
		msg->pStr = str;
	}

	if (xQueueSendToBack(queue, msg, 10) != pdPASS)
		return -2;

	return 0;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ������� ���������� ��������� msg � ������. ������� ������ ��� ������ ��
// ������������ ����������. ������������ ������ ����������� ���������.
//------------------------------------------------------------------------------
int8_t message_AddFromISR(sqMessage *msg)
{
	xQueueHandle *queue = GetMessagesQueue();
	if (queue == NULL)
		return -1;

	if (!(msg->IsDynamicImage))
		return -3;

	if (xQueueSendToBackFromISR(queue, (void *) msg, NULL) != pdTRUE)
		return -2;

	return 0;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ��������� ������� ������ ���������
//------------------------------------------------------------------------------
void message_Clear(int8_t MaxPriority)
{
	int16_t i;

	// ����������� ������ � ������ ���������
	xSemaphoreTake(xSemPool, 100);

	for (i = 0; i < MESSAGES_POOL_SIZE; i++)
	{
		if (MessagesPool[i].State == MessageState_Run)
			MessagesPool[i].TimeLeft = 0;
		else if (MessagesPool[i].State == MessageState_Idle)
			DeleteMessageFromPool(&MessagesPool[i]);
	}

		// ����������� ������ � ������ ���������
	xSemaphoreGive(xSemPool);
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ���� ������ ������������� ���������
//------------------------------------------------------------------------------
void messagetask(void *pvParameters)
{
	sqMessage Message;
	sMessage *pCurrentMessage = 0;

	// �������� ������� ��, ����� ������� ��������� ���������. ���� ��� �� ���������������� - ��������������
	xQueueHandle *queue = GetMessagesQueue();
	if (queue == NULL)
	{
		vTaskDelete(NULL);
		return;
	}

	while (!task_stopping)
	{
		// ����������� ������ � ������ ���������
		xSemaphoreTake(xSemPool, 0);

		// ��������� ����� ���������
		while (xQueueReceive(queue, &Message, 1))
		{
			// ��������� ����� ��������� � ��� ���������
			AddMessageToPool(&Message);
		}

		// ���������, �� ������� �� ����� ����������� �������� ���������
		if (pCurrentMessage)
		{
			if (pCurrentMessage->TimeLeft)		// ����� ����������� �� �������?
			{
				// ���� ��������� ������������ - ��������� ��� �� �������
				if (pCurrentMessage->IsDynamicImage)
					RunMessage(pCurrentMessage);

				// ����������� ����� ����������� ���������
				pCurrentMessage->TimeLeft--; 	// ����������� ��������� ��� ������� ����������� ���������
			}
			
			if (!pCurrentMessage->TimeLeft)		// ����� ����������� �������? 
			{
				// �������� �������� ���������
				RunOutMessage(pCurrentMessage, 0);
				// ������� ��������� �� ������
				DeleteMessageFromPool(pCurrentMessage);
				// �������� ��������� �� ������� ���������
				pCurrentMessage = 0;
			}
		}

		// �������, �� ��������� �� � ���� ��������� � ����� ������� �����������.
		// ���� �� ��������� - ���� ������ �� ������� ������������ ���������
		sMessage *pNewMessage = GetMaxPriorityIdleMessage();

		// ����� ����� ��������� � ��������� � ���� ����, ��� � �������� ���������
		if ((pNewMessage) && ((!pCurrentMessage) || (pNewMessage->Priority > pCurrentMessage->Priority)))
		{
			// � ������ ������ �������� ����� �� ���������
			if (pCurrentMessage)
			{
				// ������ �������� ���������
				RunOutMessage(pCurrentMessage, 1);
				pCurrentMessage->State = MessageState_Idle;
			}

			// ���� �� ���������� ������� ���������, �� ����� ������� ������
			if (pCurrentMessage)
			{
				// ������ ������� ���������
				RunInMessage(pNewMessage, 1);
			}
			else	// ����� ������� ����� ��������� ��������
			{
				// �������� ������� ���������
				RunInMessage(pNewMessage, 0);
			}

			pCurrentMessage = pNewMessage;
			MessageRun(pCurrentMessage);
		}

		// ����������� ������ � ������ ���������
		xSemaphoreGive(xSemPool);

		// �������� - ��� ������ ������������� ���������
		vTaskDelay(MESSAGES_SHOW_TICK);
	}

	task_run = 0;
	// ����������� ������, ������� ��� ������� �� ��� �������� ���������
	vQueueDelete(queue);
	// ��������� ��������� ������� ������
	vTaskDelete(NULL);
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ��������� ������� ������ ������������� ���������
//------------------------------------------------------------------------------
void messagetask_start(void)
{
	task_stopping = 0;

	// ������������� ������ ��������
	ani_Init();
	// �������� ������� ��� ���������� ������� � ������ ���������
	xSemPool = xSemaphoreCreateMutex();
	// ������ ������
	xTaskCreate(messagetask, "MesManager", 512/*256*/, NULL, 2, NULL);

	task_run = 1;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ��������� ��������� ������ ������������� ���������. ��������� ���������
// ������� ����� �� ������� ���������� ������.
//------------------------------------------------------------------------------
void messagetask_stop(void)
{
	task_stopping = 1;
	
	// ��� ���������� ������
	while (task_run)
	{
		vTaskDelay(MESSAGES_SHOW_TICK);
	}

	// ����������� ������, ������� ��� ������
	vSemaphoreDelete(xSemPool);
	// ���������������� ������ ��������
	ani_DeInit();
}
//------------------------------------------------------------------------------
