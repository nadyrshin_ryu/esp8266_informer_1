#ifndef SPI_APP_H
#define SPI_APP_H

#include "spi_register.h"
#include "ets_sys.h"


// SPI number define
#define SPI 			0
#define HSPI			1

void cache_flush(void);
//spi master init funtion
void spi_master_init(uint8 spi_no);
//use spi send 8bit data
void spi_mast_byte_write(uint8 spi_no,uint8 data);

#endif

