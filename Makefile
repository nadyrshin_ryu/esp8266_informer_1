# Main settings includes
include	../settings.mk

# Individual project settings (Optional)
SDK_BASE	= c:/Espressif/ESP8266_RTOS_SDK
EXTRA_INCDIR = $(SDK_BASE)/driver_lib/include
#BOOT		= new
#APP		= 1
#SPI_SPEED	= 40
#SPI_MODE	= QIO
#SPI_SIZE_MAP	= 2
#ESPPORT		= COM2
#ESPBAUD		= 256000

# Basic project settings
MODULES	= driver display clocktask animations messagetask buttontask user
LIBS	= gcc hal phy pp net80211 wpa crypto main freertos lwip minic smartconfig

# Root includes
include	../common_rtos.mk
