//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// ����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "esp_common.h"
#include "user_config.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <freertos/queue.h>

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "../driver/uart.h"
#include "../driver/gpio.h"
#include "../driver/spi.h"

#include "../display/font.h"
#include "../animations/animations.h"

#include "../clocktask/clocktask.h"
#include "../buttontask/buttontask.h"
#include "../messagetask/messagetask.h"


/******************************************************************************
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABCCC
 *                A : rf cal
 *                B : rf init data
 *                C : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
 *******************************************************************************/
uint32 user_rf_cal_sector_set(void)
{
	flash_size_map size_map = system_get_flash_size_map();
	uint32 rf_cal_sec = 0;

	switch (size_map)
	{
	case FLASH_SIZE_4M_MAP_256_256:
		rf_cal_sec = 128 - 5;
		break;

	case FLASH_SIZE_8M_MAP_512_512:
		rf_cal_sec = 256 - 5;
		break;

	case FLASH_SIZE_16M_MAP_512_512:
	case FLASH_SIZE_16M_MAP_1024_1024:
		rf_cal_sec = 512 - 5;
		break;

	case FLASH_SIZE_32M_MAP_512_512:
	case FLASH_SIZE_32M_MAP_1024_1024:
		rf_cal_sec = 1024 - 5;
		break;

	default:
		rf_cal_sec = 0;
		break;
	}

	return rf_cal_sec;
}


//------------------------------------------------------------------------------
// ���� ������ ���������� ����������� �� ESP-12
//------------------------------------------------------------------------------
void ledtask(void *pvParameters)
{
	GPIO_AS_OUTPUT(GPIO_Pin_1);

	while (1)
	{
		GPIO_OUTPUT_SET(GPIO_Pin_1, 1);
		//printf("LED ON\n");
		vTaskDelay(50);
		GPIO_OUTPUT_SET(GPIO_Pin_1, 0);
		//printf("LED OFF\n");
		vTaskDelay(50);
	}
}
//------------------------------------------------------------------------------

static uint8_t StrBuff[100];

//------------------------------------------------------------------------------
// ��������� ��������� � ������ ����� ����� ����������� ��������� (��� ��� ���� 
// ����� �������� ������ � ������ ���������)
//------------------------------------------------------------------------------
void SendStaticText(uint8_t *pBuff, uint8_t Animation, uint16_t Period, uint16_t Time, uint8_t HAlign, uint8_t Priority)
{
	sqMessage msg;
	msg.pStr = pBuff;
	msg.AnimationIn = Animation;
	msg.AnimationOut = Animation;
	msg.TickNormal = Period;
	msg.TickBraked = 0;
	msg.ShowTime = Time;
	msg.HAlign = HAlign;
	msg.Font = FONTID_6X8M;
	msg.Priority = Priority;
	msg.IsDynamicImage = 0;
	message_Add(&msg);
	printf("Static: %s\n", msg.pStr);
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ��������� ��������� � ������ ����� ����� ������������ ��������� (������ pBuff
// ����� ������ �� ����. ��� ���������� ������� � ������ ���������� ������ � ������
// ����������� ��������� ����� ������������ ������ BuffMutex. ���� ���������� 
// ������� �� �����, �� ����� �������� BuffMutex = 0.
//------------------------------------------------------------------------------
void SendDynamicText(uint8_t *pBuff, void *BuffMutex, uint8_t Animation, uint16_t Period, uint16_t Time, uint8_t HAlign, uint8_t Priority)
{
	sqMessage msg;
	msg.pStr = pBuff;
	msg.Mutex = BuffMutex;
	msg.AnimationIn = Animation;
	msg.AnimationOut = Animation;
	msg.TickNormal = Period;
	msg.TickBraked = 0;
	msg.ShowTime = Time;
	msg.HAlign = HAlign;
	msg.Font = FONTID_6X8M;
	msg.Priority = Priority;
	msg.IsDynamicImage = 1;
	message_Add(&msg);
	printf("Dynamic: %s\n", msg.pStr);
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ��������� �������� ������� ������. ���� ������� �� ���������� � ������� 
// TimeToWait �����, �� ��������� ������������ ������� �����.
//------------------------------------------------------------------------------
void ButtonWaitInWelcomeMode(uint32_t TimeToWait)
{
	uint8_t Time;

	if (xQueueReceive(qButton, &Time, TimeToWait))
	{
		if (Time == BUTTON_Code_ShortPress)	// ���� �������� ������� �� ������
		{
			SendDynamicText(TimeBuff, xSemTimeBuff, 14, 2, 7, HALIGN_CENTER, 2);
		}
		else								// ���� ������� ������� �� ������
		{
			sprintf(StrBuff, "����� ������� ���������\n");
			SendStaticText(StrBuff, 14, 2, 1, HALIGN_CENTER, 3);
		}
	}
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ���� �������� ������ (� ����� ������ ����� ����������� �� ������ ������)
//------------------------------------------------------------------------------
void modestask(void *pvParameters)
{
	sqMessage message;
	int16_t StrLen;

	// ������������� ����������� �����������
	disp1color_Init();
	disp1color_SetBrightness(15);

	// ���� ������ ����������� �� ���������� ������ (�� �������, ������ ���� ���������� ��������� youtube-������)
	while (1)
	{
		// �� �������
		sprintf(StrBuff, "������");
		SendStaticText(StrBuff, 1, 1, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� ����������
		sprintf(StrBuff, "������");
		SendStaticText(StrBuff, 3, 2, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� �����������
		sprintf(StrBuff, "����������");
		SendStaticText(StrBuff, 2, 1, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� ���������
		sprintf(StrBuff, "Salam");
		SendStaticText(StrBuff, 4, 2, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� ��������
		sprintf(StrBuff, "Guten Tag");
		SendStaticText(StrBuff, 5, 1, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� ���������
		sprintf(StrBuff, "Sveiki");
		SendStaticText(StrBuff, 6, 1, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� ���������
		sprintf(StrBuff, "Buna");
		SendStaticText(StrBuff, 7, 1, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� ���������
		sprintf(StrBuff, "Sveikas");
		SendStaticText(StrBuff, 8, 2, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� ��������
		sprintf(StrBuff, "Marhaba");
		SendStaticText(StrBuff, 9, 3, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� ����������
		sprintf(StrBuff, "Hello");
		SendStaticText(StrBuff, 10, 4, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� ���������
		sprintf(StrBuff, "Tervist");
		SendStaticText(StrBuff, 11, 1, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� ��������
		sprintf(StrBuff, "Dzien dobry");
		SendStaticText(StrBuff, 12, 3, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� ����������
		sprintf(StrBuff, "Gamardjobat");
		SendStaticText(StrBuff, 13, 2, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
		// �� ����������
		sprintf(StrBuff, "Zdraveite");
		SendStaticText(StrBuff, 14, 2, 2, HALIGN_CENTER, 1);
		ButtonWaitInWelcomeMode(400);
	}
	
	// ������� ������ ���������
	message_Clear(MESSAGES_MAX_PRIORITY);
	// ����������������� ����������� ����������
	disp1color_Deinit();
	// ��������� ��������� ������� ������
	vTaskDelete(NULL);
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// �������� ��������� �� ������� ���������� ������������� � ����� �����
//------------------------------------------------------------------------------
void user_init(void)
{
	// ������ baudrate UART0. ��-��������� ����� �������� ���������
	UART_SetBaudrate(UART0, BIT_RATE_115200);

	printf("SDK version:%s\n", system_get_sdk_version());

	// ������������� ����� � ��������� ����������� �� wifi
	wifi_set_opmode(STATIONAP_MODE);
	{
		struct station_config *config = (struct station_config *) zalloc(sizeof(struct station_config));
		sprintf(config->ssid, "SSID");
		sprintf(config->password, "password");

		// need to sure that you are in station mode first, otherwise it will be failed.
		wifi_station_set_config(config);
		free(config);
	}

	// ����� ������ ����� �������
	clocktask_start();
	// ����� ������ ������ ������
	buttontask_start();
	// ����� ������, ����������� ������������ ��������� �� �������
	messagetask_start();
	// ����� �������� ������
	xTaskCreate(modestask, "ModesTask", 512, NULL, 1, NULL);
	// ����� ������ ���������� ����������� �� ESP-12
	xTaskCreate(ledtask, "LedTask", 128, NULL, 2, NULL);
}
//------------------------------------------------------------------------------

