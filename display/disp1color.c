//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <disp1color.h>
#include <font.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>


#if (DISP1COLOR_type == DISPTYPE_ssd1306)
#include <ssd1306.h>
#elif (DISP1COLOR_type == DISPTYPE_DMD_1color)
#include <dmd_1color.h>
#elif (DISP1COLOR_type == DISPTYPE_st7920)
#include <st7920.h>
#elif (DISP1COLOR_type == DISPTYPE_max7219)
#include <max7219.h>
#else
#error ������ c ��������� ����� ������� �� ��������������, ���������� �������� ������ disp1color
#endif


// ��������� �� ����� �����
static uint8_t *disp1color_buff;
// ��������� �� ��������� �����
static uint8_t *disp1color_TempBuff;
static uint16_t disp1color_TempBuffWidth;


#if (DISP1COLOR_AUTOSIZE)			// � �������� ���������� ���� ������� ��� ��������������� ���������� ����������
uint16_t DISP1COLOR_Width = 8;
uint16_t DISP1COLOR_Height = 8;
#else								// ������� ������� �������������
// ������� ������� � ��������
uint16_t DISP1COLOR_Width = DISP1COLOR_Width_const;
uint16_t DISP1COLOR_Height = DISP1COLOR_Height_const;
#endif


//==============================================================================
// ��������� �������������� 1-������ �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR disp1color_Init(void)
{
#if (DISP1COLOR_type == DISPTYPE_ssd1306)
	// ������������� �������
	SSD1306_Init(DISP1COLOR_Width, DISP1COLOR_Height);
	// ������� �������
	disp1color_FillScreenbuff(0);
	SSD1306_DisplayFullUpdate(disp1color_buff, sizeof(disp1color_buff));
#elif (DISP1COLOR_type == DISPTYPE_DMD_1color)
	DMD_1color_Init(DISP1COLOR_Width, DISP1COLOR_Height);
#elif (DISP1COLOR_type == DISPTYPE_st7920)
	ST7920_Init(DISP1COLOR_Width, DISP1COLOR_Height);
#elif (DISP1COLOR_type == DISPTYPE_max7219)
	max7219_init(&DISP1COLOR_Width, &DISP1COLOR_Height);

#if (DISP1COLOR_AUTOSIZE)
#ifndef MAX7219_DetectNum
#error � �������� ���������� ��������� ��������������� ��������!
#endif
#endif

	// �������� ������ ��� ������ �����
	uint16_t ScreenBuffSize = DISP1COLOR_Width * (DISP1COLOR_Height >> 3);
	disp1color_buff = malloc(ScreenBuffSize);
	if (!disp1color_buff)	// ������ ��������� ������
		return -1;

	disp1color_TempBuff = disp1color_buff;
	disp1color_TempBuffWidth = ScreenBuffSize;

	// ������� �������
	disp1color_FillScreenbuff(0);
	disp1color_UpdateFromBuff();
#endif
	return 0;
}
//==============================================================================


//==============================================================================
// ��������� ���������������� ���������� (����������� ������)
//==============================================================================
void ICACHE_FLASH_ATTR disp1color_Deinit(void)
{
	if (disp1color_buff)
		free(disp1color_buff);
}
//==============================================================================


//==============================================================================
// ��������� ��������� ������� Test �������
//==============================================================================
void ICACHE_FLASH_ATTR disp1color_TestMode(uint8_t TestOn)
{
#if (DISP1COLOR_type == DISPTYPE_ssd1306)
	if (TestOn)
	SSD1306_AllPixOn();
	else
	SSD1306_AllPixRAM();
#elif (DISP1COLOR_type == DISPTYPE_max7219)
	max7219_set_testmode_onoff(MAX7219_ALL_IDX, TestOn);
#endif
}
//==============================================================================


//==============================================================================
// ��������� ������������� ������� �������
//==============================================================================
void ICACHE_FLASH_ATTR disp1color_SetBrightness(uint8_t Value)
{
#if (DISP1COLOR_type == DISPTYPE_ssd1306)
	SSD1306_SetContrast(Value);
#elif (DISP1COLOR_type == DISPTYPE_max7219)
	max7219_set_intensity(MAX7219_ALL_IDX, Value);
#endif
}
//==============================================================================


//==============================================================================
// ��������� ��������� ����� ����� ��������� FillValue
//==============================================================================
void ICACHE_FLASH_ATTR disp1color_FillScreenbuff(uint8_t FillValue)
{
	memset(disp1color_TempBuff, FillValue, disp1color_TempBuffWidth);
}
//==============================================================================


//==============================================================================
// ��������� ��������� ��������� ����������� � ������������ � ������� ����� disp1color_buff
//==============================================================================
void /*ICACHE_FLASH_ATTR*/ disp1color_UpdateFromBuff(void)
{
#if (DISP1COLOR_type == DISPTYPE_ssd1306)
	SSD1306_DisplayFullUpdate(disp1color_buff, sizeof(disp1color_buff));
#elif (DISP1COLOR_type == DISPTYPE_DMD_1color)
	DMD_1COLOR_DisplayFullUpdate(disp1color_buff, sizeof(disp1color_buff));
#elif (DISP1COLOR_type == DISPTYPE_st7920)
	ST7920_DisplayFullUpdate(disp1color_buff, sizeof(disp1color_buff));
#elif (DISP1COLOR_type == DISPTYPE_max7219)
	max7219_update_from_buff(disp1color_TempBuff, disp1color_TempBuffWidth);
#endif
}
//==============================================================================


//==============================================================================
// ��������� ��������� ��������� ����������� � ������������ � ������� ����� disp1color_buff
//==============================================================================
void ICACHE_FLASH_ATTR disp1color_Update(uint8_t *Buff, uint16_t BuffLen)
{
	max7219_update_from_buff(Buff, BuffLen);
}
//==============================================================================


//==============================================================================
// ��������� ������������� ��������� 1 ������� �������
//==============================================================================
void disp1color_DrawPixel(int16_t X, int16_t Y, uint8_t State)
{
	// ���������, ��������� �� ����� � ���� ��������� �������
	if ((X >= DISP1COLOR_Width) || (Y >= DISP1COLOR_Height) || (X < 0)
			|| (Y < 0))
		return;

	uint16_t ByteIdx = Y >> 3;
	uint8_t BitIdx = Y - (ByteIdx << 3); // ������ ������������ ������ ���� (0<=Y<=7)
	ByteIdx *= DISP1COLOR_Width;
	ByteIdx += X;

	if (State)
		disp1color_TempBuff[ByteIdx] |= (1 << BitIdx);
	else
		disp1color_TempBuff[ByteIdx] &= ~(1 << BitIdx);
}
//==============================================================================


//==============================================================================
// ��������� ������ ������ ����� � ������ ����� �������
//==============================================================================
void /*ICACHE_FLASH_ATTR*/ disp1color_DrawLine(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t State)
{
	const int16_t deltaX = abs(x2 - x1);
	const int16_t deltaY = abs(y2 - y1);
	const int16_t signX = x1 < x2 ? 1 : -1;
	const int16_t signY = y1 < y2 ? 1 : -1;

	int16_t error = deltaX - deltaY;

	disp1color_DrawPixel(x2, y2, State);

	while (x1 != x2 || y1 != y2)
	{
		disp1color_DrawPixel(x1, y1, State);
		const int16_t error2 = error * 2;

		if (error2 > -deltaY)
		{
			error -= deltaY;
			x1 += signX;
		}
		if (error2 < deltaX)
		{
			error += deltaX;
			y1 += signY;
		}
	}
}
//==============================================================================


//==============================================================================
// ������� ���������� ��������� ������� �� ����� pBuff � ������� ��������� ������
//==============================================================================
uint8_t disp1color_getPixel(uint8_t *pBuff, int16_t X, int16_t Y, int16_t W, int16_t H)
{
	int16_t byteidx = Y >> 3;	// � ������
	byteidx *= W;				// � ����� � ������ ��� ����� ������ � ������
	byteidx += X;				// � ����� � ������
	uint8_t bit = Y & 0x07;		// � ������ �������� � ������ ��������

	if (pBuff[byteidx] & (1 << bit))
		return 1;
	else
		return 0;
}
//==============================================================================


//==============================================================================
// ���������� ������������� ����� �� ������ pBuff � ����� �����
//==============================================================================
void disp1color_DrawBuffer(int16_t X, int16_t Y, uint8_t *pBuff, int16_t BuffX, int16_t BuffY, int16_t BuffW, int16_t BuffH)
{
	int16_t x, y;			// ���������� � �������� ������

	for (y = 0; y < BuffH; y++)
	{
		for (x = 0; x < BuffW; x++)
		{
			if (disp1color_getPixel(pBuff, x + BuffX, y + BuffY, x + BuffW, y + BuffH))
				disp1color_DrawPixel(X + x, Y + y, 1);
			else
				disp1color_DrawPixel(X + x, Y + y, 0);
		}
	}

}
//==============================================================================


//==============================================================================
// ��������� ������ ������������� � ������ ����� �������
//==============================================================================
void ICACHE_FLASH_ATTR disp1color_DrawRectangle(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t State)
{
	disp1color_DrawLine(x1, y1, x1, y2, State);
	disp1color_DrawLine(x2, y1, x2, y2, State);
	disp1color_DrawLine(x1, y1, x2, y1, State);
	disp1color_DrawLine(x1, y2, x2, y2, State);
}
//==============================================================================


//==============================================================================
// ��������� ������ ���������� � ������ ����� �������. x0 � y0 - ���������� ������ ����������
//==============================================================================
void ICACHE_FLASH_ATTR disp1color_DrawCircle(int16_t x0, int16_t y0, int16_t radius)
{
	int x = 0;
	int y = radius;
	int delta = 1 - 2 * radius;
	int error = 0;

	while (y >= 0)
	{
		disp1color_DrawPixel(x0 + x, y0 + y, 1);
		disp1color_DrawPixel(x0 + x, y0 - y, 1);
		disp1color_DrawPixel(x0 - x, y0 + y, 1);
		disp1color_DrawPixel(x0 - x, y0 - y, 1);
		error = 2 * (delta + y) - 1;

		if (delta < 0 && error <= 0)
		{
			++x;
			delta += 2 * x + 1;
			continue;
		}

		error = 2 * (delta - x) - 1;

		if (delta > 0 && error > 0)
		{
			--y;
			delta += 1 - 2 * y;
			continue;
		}

		++x;
		delta += 2 * (x - y);
		--y;
	}
}
//==============================================================================


//==============================================================================
// ��������� ������� �� ������� ��������������� ������
//==============================================================================
void ICACHE_FLASH_ATTR disp1color_printf(int16_t X, int16_t Y, uint8_t FontID, const char *args, ...)
{
	char StrBuff[200];

	va_list ap;
	va_start(ap, args);
	char len = vsnprintf(StrBuff, sizeof(StrBuff), args, ap);
	va_end(ap);

	disp1color_DrawString(X, Y, FontID, (uint8_t *) StrBuff);
}
//==============================================================================


//==============================================================================
// ������� ��������� ������ � ��������� ����� Buff � ������� ��������� ������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ConvertText2TempBuff(uint8_t *Str, uint8_t *Buff, uint16_t BuffLen, uint8_t FontID)
{
	disp1color_ChangeScreenBuff(Buff, BuffLen);

	int16_t Width = disp1color_DrawString(0, 0, FontID, Str);

	disp1color_ChangeScreenBuff(0, 0);

	return Width;
}
//==============================================================================


//==============================================================================
// ������� ������ ������� Char �� �������. ���������� ������ ����������� �������
//==============================================================================
uint8_t disp1color_DrawChar(int16_t X, int16_t Y, uint8_t FontID, uint8_t Char)
{
	uint8_t row, col;

	// ��������� �� ����������� ����������� ������� ������
	uint8_t *pCharTable = font_GetFontStruct(FontID, Char);
	uint8_t CharWidth = font_GetCharWidth(pCharTable);    // ������ �������
	uint8_t CharHeight = font_GetCharHeight(pCharTable);  // ������ �������
	pCharTable += 2;

	if (FontID == FONTID_6X8M)
	{
		for (row = 0; row < CharHeight; row++)
		{
			for (col = 0; col < CharWidth; col++)
				disp1color_DrawPixel(X + col, Y + row,
						pCharTable[row] & (1 << (7 - col)));
		}
	}
	else
	{
		for (row = 0; row < CharHeight; row++)
		{
			for (col = 0; col < CharWidth; col++)
			{
				if (col < 8)
					disp1color_DrawPixel(X + col, Y + row,
							pCharTable[row * 2] & (1 << (7 - col)));
				else
					disp1color_DrawPixel(X + col, Y + row,
							pCharTable[(row * 2) + 1] & (1 << (15 - col)));
			}
		}
	}

	return CharWidth;
}
//==============================================================================

//==============================================================================
// ������� ������ ������ �� ������ Str �� �������
//==============================================================================
int16_t disp1color_DrawString(int16_t X, int16_t Y, uint8_t FontID, uint8_t *Str)
{
	uint8_t done = 0;             // ���� ��������� ������
	int16_t Xstart = X; // ���������� ���� ����� ���������� ������� ��� �������� �� ����� ������
	uint8_t StrHeight = 8; // ������ �������� � �������� ��� �������� �� ��������� ������

	// ����� ������
	while (!done)
	{
		switch (*Str)
		{
		case '\0':  // ����� ������
			done = 1;
			break;
		case '\n':  // ������� �� ��������� ������
			Y += StrHeight;
			break;
		case '\r':  // ������� � ������ ������
			X = Xstart;
			break;
		default:    // ������������ ������
			X += disp1color_DrawChar(X, Y, FontID, *Str);
			StrHeight = font_GetCharHeight(font_GetFontStruct(FontID, *Str));
			break;
		}

		Str++;
	}
	return X;// ? X - 1 : 0;
}
//==============================================================================


//==============================================================================
// ������� ������� ��������� �� ����� �����
//==============================================================================
uint8_t ICACHE_FLASH_ATTR *disp1color_ChangeScreenBuff(uint8_t *Buff, uint16_t BuffLen)
{
	if ((!Buff) || (!BuffLen))
	{
		disp1color_TempBuff = disp1color_buff;
		disp1color_TempBuffWidth = sizeof(disp1color_buff);
	}
	else
	{
		disp1color_TempBuff = Buff;
		disp1color_TempBuffWidth = BuffLen;
	}

	return disp1color_TempBuff;
}
//==============================================================================
