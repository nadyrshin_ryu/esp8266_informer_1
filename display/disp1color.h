//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _DISP1COLOR_H
#define _DISP1COLOR_H

#include <stdint.h>

// ������ �������������� �������� (������������ ��������)
#define DISPTYPE_ssd1306        0       // OLED-������� � ������������ ssd1306
#define DISPTYPE_DMD_1color     1       // LED-������� ����������� �� ������
#define DISPTYPE_st7920         2       // LCD-������� � ������������ st7920
#define DISPTYPE_max7219     	3       // LED-������� �� max7219


// � ����� ����� ������� ����� �������� ������ disp1color 
#define DISP1COLOR_type         DISPTYPE_max7219

#define DISP1COLOR_AUTOSIZE		1

// ����������, ���������� ������� ������� � ��������
extern uint16_t DISP1COLOR_Width, DISP1COLOR_Height;

#if (DISP1COLOR_AUTOSIZE)			// � �������� ���������� ���� ������� ��� ��������������� ���������� ����������

#else								// ������� ������� �������������
// ������� ������� � ��������
#define DISP1COLOR_Width_const  20*8
#define DISP1COLOR_Height_const 8
#endif



// ��������� �������������� 1-������ �������
int16_t disp1color_Init(void);
// ��������� ���������������� ���������� (����������� ������)
void disp1color_Deinit(void);
// ��������� ��������� ������� Test �������
void disp1color_TestMode(uint8_t TestOn);
// ��������� ������������� ������� �������
void disp1color_SetBrightness(uint8_t Value);
// ��������� ��������� ����� ����� ��������� FillValue
void disp1color_FillScreenbuff(uint8_t FillValue);
// ��������� ��������� ��������� ����������� � ������������ � ������� �����
void disp1color_UpdateFromBuff(void);
// ��������� ������ ������ ����� � ������ ����� �������
void disp1color_DrawLine(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t State);
// ��������� ������ ������������� � ������ ����� �������
void disp1color_DrawRectangle(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t State);
// ��������� ������ ���������� � ������ ����� �������. x0 � y0 - ���������� ������ ����������
void disp1color_DrawCircle(int16_t x0, int16_t y0, int16_t radius);
// ������� ������ ������� Char �� �������. ���������� ������ ����������� �������
uint8_t disp1color_DrawChar(int16_t X, int16_t Y, uint8_t FontID, uint8_t Char);
// ������� ������ ������ �� ������ Str �� �������
int16_t disp1color_DrawString(int16_t X, int16_t Y, uint8_t FontID, uint8_t *Str);
// ��������� ������� �� ������� ��������������� ������
void disp1color_printf(int16_t X, int16_t Y, uint8_t FontID, const char *args, ...);
// ������� ��������� ������ � ��������� ����� Buff � ������� ��������� ������
int16_t ConvertText2TempBuff(uint8_t *Str, uint8_t *Buff, uint16_t BuffLen, uint8_t FontID);
// ������� ������� ��������� �� ����� �����
uint8_t *disp1color_ChangeScreenBuff(uint8_t *Buff, uint16_t BuffLen);


#endif
