//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <freertos/FreeRTOS.h>
#include <freertos/FreeRTOSConfig.h>
#include "freertos/task.h"
#include <disp1color.h>
#include <animations.h>
#include <ani14.h>


#define CHAR_STEP		5
#define STEP_PER_CHAR	8


//==============================================================================
// �������, ����������� �������� ��������� ������� �� �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani14_RunIn(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t i, j;
	int16_t x = GetXposAligned(BuffLen, H_Align);
	int16_t y = 7;

	uint8_t Chars = BuffLen / 6;
	if (BuffLen % 6)
		Chars++;
	uint8_t Steps[Chars];

	for (i = 0; i < Chars; i++)
		Steps[i] = 0;

	for (i = 0; i < (CHAR_STEP * Chars) + STEP_PER_CHAR; i++)
	{
		ani_SemTake();

		for (j = 0; j < Chars; j++)
		{
			if (i > (CHAR_STEP * j))
			{
				if (Steps[j] < STEP_PER_CHAR)
				{
					// ��������� ����������� � ����� �����
					disp1color_DrawBuffer(x + (j * 6), y - Steps[j], TempBuff, (j * 6), 0, 6, 8);
					Steps[j]++;
				}
			}
		}

		ani_UpdateDisplay();
		ani_SemGive();

		// �������� ����� ��������
		vTaskDelay(Period);
	}

	return 0;
}
//==============================================================================


//==============================================================================
// �������, ����������� �������� ������������ ������� � �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani14_RunOut(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t i, j;
	int16_t x = GetXposAligned(BuffLen, H_Align);
	int16_t y = -1;

	uint8_t Chars = BuffLen / 6;
	if (BuffLen % 6)
		Chars++;
	uint8_t Steps[Chars];

	for (i = 0; i < Chars; i++)
		Steps[i] = 0;

	for (i = 0; i < (CHAR_STEP * Chars) + STEP_PER_CHAR; i++)
	{
		ani_SemTake();

		for (j = 0; j < Chars; j++)
		{
			if (i > (CHAR_STEP * j))
			{
				if (Steps[j] < STEP_PER_CHAR)
				{
					// ��������� ����������� � ����� �����
					disp1color_DrawBuffer(x + (j * 6), y - Steps[j], TempBuff, j * 6, 0, 6, 8);
					disp1color_DrawLine(x + (j * 6), y - Steps[j] + 8, x + ((j + 1) * 6) - 1, y - Steps[j] + 8, 0);

					Steps[j]++;
				}
			}
		}

		ani_UpdateDisplay();
		ani_SemGive();

		// �������� ����� ��������
		vTaskDelay(Period);
	}

	return 0;
}
//==============================================================================
