//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <string.h>
#include <disp1color.h>
#include <animations.h>
#include <ani0.h>
#include <ani1.h>
#include <ani2.h>
#include <ani3.h>
#include <ani4.h>
#include <ani5.h>
#include <ani6.h>
#include <ani7.h>
#include <ani8.h>
#include <ani9.h>
#include <ani10.h>
#include <ani11.h>
#include <ani12.h>
#include <ani13.h>
#include <ani14.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <freertos/semphr.h>


// ������� � ����������� �� ������� RunIn ������ ��������
static const t_ani_runin ani_runin_funcs[] =
{
		  ani0_RunIn,
		  ani1_RunIn,
		  ani2_RunIn,
		  ani3_RunIn,
		  ani4_RunIn,
		  ani5_RunIn,
		  ani6_RunIn,
		  ani7_RunIn,
		  ani8_RunIn,
		  ani9_RunIn,
		  ani10_RunIn,
		  ani11_RunIn,
		  ani12_RunIn,
		  ani13_RunIn,
		  ani14_RunIn
};
// ������� � ����������� �� ������� RunOut ������ ��������
static const t_ani_runout ani_runout_funcs[] =
{
		  ani0_RunOut,
		  ani1_RunOut,
		  ani2_RunOut,
		  ani3_RunOut,
		  ani4_RunOut,
		  ani5_RunOut,
		  ani6_RunOut,
		  ani7_RunOut,
		  ani8_RunOut,
		  ani9_RunOut,
		  ani10_RunOut,
		  ani11_RunOut,
		  ani12_RunOut,
		  ani13_RunOut,
		  ani14_RunOut
};

// ������� (������������ ��� ������) ��� ������������� ������� �� ������ �������
// � ���������� � ��� ������ �����  ����� ������������� ��������� ��� ������������� ������� �� ���������� �����
static xQueueHandle xSemDisplay;

//==============================================================================
// ��������� ������������� ������ ��������
//==============================================================================
void ani_Init(void)
{
	xSemDisplay = xSemaphoreCreateMutex();
}
//==============================================================================


//==============================================================================
// ��������� ���������������� ������ ��������.
// ������ ���������� ������ ����� ��������� ���� ��������
//==============================================================================
void ani_DeInit(void)
{
	// ���������� ������
	vSemaphoreDelete(xSemDisplay);
}
//==============================================================================


//==============================================================================
// ��������� ������� ������� � ���������� � ��� ������ �����
//==============================================================================
void ani_SemTake()
{
	xSemaphoreTake(xSemDisplay, 0);
}
//==============================================================================


//==============================================================================
// ��������� ������������ ������� � ���������� � ��� ������ �����
//==============================================================================
void ani_SemGive()
{
	xSemaphoreGive(xSemDisplay);
}
//==============================================================================


//==============================================================================
// ������� ����������� ���������� X ��� ��������� ������� � ������ ������������ �� �����������
//==============================================================================
int16_t GetXposAligned(uint16_t BuffLen, uint8_t H_Align)
{
	switch (H_Align)
	{
	case HALIGN_LEFT:
		return 0;
	case HALIGN_RIGHT:
		return DispWidth - BuffLen;
	default:
		return (DispWidth >> 1) - (BuffLen >> 1);
	}
}
//==============================================================================


//==============================================================================
// ������� ������ ����������� � ������������� (��� ��������)
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani_Run(uint8_t *NewFrameBuff, uint16_t BuffLen, uint8_t H_Align)
{
	uint8_t *DispBuff = disp1color_ChangeScreenBuff(0, 0);

	int16_t x = GetXposAligned(BuffLen, H_Align);

	ani_SemTake();

	disp1color_DrawBuffer(x, 0, NewFrameBuff, 0, 0, BuffLen, 8);
	ani_UpdateDisplay();

	ani_SemGive();

	return 0;
}
//==============================================================================


//==============================================================================
// ������� ��������� ��������� �������� ��������� �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani_RunIn(uint8_t effect, uint8_t *NewFrameBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	if (effect >= (sizeof(ani_runin_funcs) / sizeof(t_ani_runin)))
		return -2;

	uint8_t *DispBuff = disp1color_ChangeScreenBuff(0, 0);
	return ani_runin_funcs[effect](DispBuff, NewFrameBuff, BuffLen, H_Align, Period);
}
//==============================================================================


//==============================================================================
// ������� ��������� ��������� �������� ������������ �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani_RunOut(uint8_t effect, uint8_t *NewFrameBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	if (effect > (sizeof(ani_runout_funcs) / sizeof(t_ani_runout)))
		return -2;

	uint8_t *DispBuff = disp1color_ChangeScreenBuff(0, 0);
	return ani_runout_funcs[effect](DispBuff, NewFrameBuff, BuffLen, H_Align, Period);
}
//==============================================================================


//==============================================================================
// ��������� ���������� ���������� �� ������ �����
//==============================================================================
void /*ICACHE_FLASH_ATTR*/ ani_UpdateDisplay(void)
{
	disp1color_ChangeScreenBuff(0, 0);
	disp1color_UpdateFromBuff();
}
//==============================================================================
