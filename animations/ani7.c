//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <freertos/FreeRTOS.h>
#include <freertos/FreeRTOSConfig.h>
#include <disp1color.h>
#include <animations.h>
#include <ani7.h>


//==============================================================================
// �������, ����������� �������� ��������� ������� �� �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani7_RunIn(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t x = GetXposAligned(BuffLen, H_Align);
	int16_t yslider = 0;

	// ���������
	for (; yslider <= 8; yslider++)
	{
		ani_SemTake();

		disp1color_DrawLine(x, yslider, x + BuffLen - 1, yslider, yslider == 8 ? 0 : 1);
		disp1color_DrawBuffer(x, yslider - 1, TempBuff, 0, yslider - 1, BuffLen, 1);
		ani_UpdateDisplay();

		ani_SemGive();

		vTaskDelay(Period * 2);
	}

	return 0;
}
//==============================================================================


//==============================================================================
// �������, ����������� �������� ������������ ������� � �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani7_RunOut(uint8_t *DispBuff, uint8_t *TempBuff,
		uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t x = GetXposAligned(BuffLen, H_Align);
	int16_t yslider = 0;

	// ������������
	for (; yslider <= 8; yslider++)
	{
		ani_SemTake();

		disp1color_DrawLine(x, yslider, x + BuffLen - 1, yslider, 1);
		disp1color_DrawLine(x, yslider - 1, x + BuffLen - 1, yslider - 1, 0);
		ani_UpdateDisplay();

		ani_SemGive();

		vTaskDelay(Period * 2);
	}

	return 0;
}
//==============================================================================
