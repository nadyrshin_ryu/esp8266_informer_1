//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef ANIMATIONS_ANI13_H_
#define ANIMATIONS_ANI13_H_


// �������, ����������� �������� ��������� ������� �� �������
int16_t ani13_RunIn(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period);
// �������, ����������� �������� ������������ ������� � �������
int16_t ani13_RunOut(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period);

#endif /* ANIMATIONS_ANI13_H_ */
