//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <freertos/FreeRTOS.h>
#include <freertos/FreeRTOSConfig.h>
#include <disp1color.h>
#include <animations.h>
#include <ani11.h>


#define ANI11_STEP_PIXELS	40

//==============================================================================
// �������, ����������� �������� ��������� ������� �� �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani11_RunIn(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t x = GetXposAligned(BuffLen, H_Align);
	int16_t Step = 0;
	int16_t StepNum = BuffLen << 1;

	// ���������
	for (; Step < StepNum; Step++)
	{
		ani_SemTake();

		int16_t X, Y, i;
		for (i = 0; i < ANI11_STEP_PIXELS; i++)
		{
			X = os_random() % BuffLen;
			Y = os_random() & 0x07;
			disp1color_DrawBuffer(x + X, Y, TempBuff, X, Y, 1, 1);
		}

		ani_UpdateDisplay();

		ani_SemGive();

		vTaskDelay(Period * 2);
	}

	return 0;
}
//==============================================================================


//==============================================================================
// �������, ����������� �������� ������������ ������� � �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani11_RunOut(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t x = GetXposAligned(BuffLen, H_Align);
	int16_t Step = 0;
	int16_t StepNum = BuffLen << 1;

	// ������������
	for (; Step < StepNum; Step++)
	{
		ani_SemTake();

		int16_t X, Y, i;
		for (i = 0; i < ANI11_STEP_PIXELS; i++)
		{
			X = os_random() % BuffLen;
			Y = os_random() & 0x07;
			disp1color_DrawPixel(x + X, Y, 0);
		}

		ani_UpdateDisplay();

		ani_SemGive();

		vTaskDelay(Period * 2);
	}

	return 0;
}
//==============================================================================
