//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <freertos/FreeRTOS.h>
#include <freertos/FreeRTOSConfig.h>
#include <disp1color.h>
#include <animations.h>
#include <ani3.h>


//==============================================================================
// �������, ����������� �������� ��������� ������� �� �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani3_RunIn(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t x = GetXposAligned(BuffLen, H_Align);
	int16_t y = -8;
	int16_t newy = 0;

	// ���������
	for (; y <= newy; y++)
	{
		ani_SemTake();

		disp1color_DrawBuffer(x, y, TempBuff, 0, 0, BuffLen, 8);
		disp1color_DrawLine(x, y - 1, x + BuffLen, y - 1, 0);
		ani_UpdateDisplay();

		ani_SemGive();

		vTaskDelay(Period * 2);
	}

	return 0;
}
//==============================================================================


//==============================================================================
// �������, ����������� �������� ������������ ������� � �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani3_RunOut(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t x = GetXposAligned(BuffLen, H_Align);
	int16_t y = 0;
	int16_t newy = 8;

	// ������������
	for (; y <= newy; y++)
	{
		ani_SemTake();

		disp1color_DrawBuffer(x, y, TempBuff, 0, 0, BuffLen, 8);
		disp1color_DrawLine(x, y - 1, x + BuffLen, y - 1, 0);
		ani_UpdateDisplay();

		ani_SemGive();

		vTaskDelay(Period * 2);
	}

	return 0;
}
//==============================================================================
