//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <freertos/FreeRTOS.h>
#include <freertos/FreeRTOSConfig.h>
#include <disp1color.h>
#include <animations.h>
#include <ani10.h>


//==============================================================================
// �������, ����������� �������� ��������� ������� �� �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani10_RunIn(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t HalfWidth = BuffLen >> 1;
	int16_t x = GetXposAligned(BuffLen, H_Align) + HalfWidth;
	int16_t xslider = HalfWidth;

	// ���������
	for (; xslider >= 0; xslider--)
	{
		ani_SemTake();

		if (xslider)
		{
			disp1color_DrawLine(x - xslider + 1, 0, x - xslider + 1, 7, xslider == 0 ? 0 : 1);
			if ((HalfWidth + xslider) < BuffLen)
				disp1color_DrawLine(x + xslider - 1, 0, x + xslider - 1, 7, xslider == 0 ? 0 : 1);
		}

		//disp1color_DrawBuffer(x - HalfWidth, 0, TempBuff, 0, 0, HalfWidth - xslider, 8);

		disp1color_DrawBuffer(x - xslider, 0, TempBuff, HalfWidth - xslider, 0, 1, 8);
		if ((HalfWidth + xslider) < BuffLen)
			disp1color_DrawBuffer(x + xslider, 0, TempBuff, HalfWidth + xslider, 0, 1, 8);

		ani_UpdateDisplay();

		ani_SemGive();

		vTaskDelay(Period);
	}

	return 0;
}
//==============================================================================


//==============================================================================
// �������, ����������� �������� ������������ ������� � �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani10_RunOut(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t HalfWidth = BuffLen >> 1;
	int16_t x = GetXposAligned(BuffLen, H_Align) + HalfWidth;
	int16_t xslider = HalfWidth;

	// ������������
	for (; xslider >= 0; xslider--)
	{
		ani_SemTake();

		disp1color_DrawLine(x + xslider, 0, x + xslider, 7, xslider == HalfWidth ? 0 : 1);
		disp1color_DrawLine(x - xslider, 0, x - xslider, 7, xslider == HalfWidth ? 0 : 1);

		if (xslider != HalfWidth)
		{
			disp1color_DrawLine(x + xslider + 1, 0, x + xslider + 1, 7, 0);
			disp1color_DrawLine(x - xslider - 1, 0, x - xslider - 1, 7, 0);
		}

		if (!xslider)
			disp1color_DrawLine(x, 0, x, 7, 0);

		ani_UpdateDisplay();

		ani_SemGive();

		vTaskDelay(Period);
	}

	return 0;
}
//==============================================================================
