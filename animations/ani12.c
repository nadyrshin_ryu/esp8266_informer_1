//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <freertos/FreeRTOS.h>
#include <freertos/FreeRTOSConfig.h>
#include <disp1color.h>
#include <animations.h>
#include <ani12.h>


//==============================================================================
// �������, ����������� �������� ��������� ������� �� �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani12_RunIn(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t x = GetXposAligned(BuffLen, H_Align);
	int16_t Step = 0;
	int16_t StepNum = BuffLen / 6;

	// ���������
	for (; Step < StepNum; Step++)
	{
		ani_SemTake();

		disp1color_DrawBuffer(x + (Step * 6), 0, TempBuff, Step * 6, 0, 6, 8);
		ani_UpdateDisplay();

		ani_SemGive();

		vTaskDelay(Period * 2);
	}

	return 0;
}
//==============================================================================


//==============================================================================
// �������, ����������� �������� ������������ ������� � �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani12_RunOut(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t x = GetXposAligned(BuffLen, H_Align);
	int16_t Step = 0;
	int16_t StepNum = BuffLen / 6;

	// ������������
	for (; Step < StepNum; Step++)
	{
		ani_SemTake();

		int16_t i, j;
		for (i = 0; i < 8; i++)
		{
			for (j = 0; j < 6; j++)
				disp1color_DrawPixel(x + (Step * 6) + j, i, 0);
		}
		ani_UpdateDisplay();

		ani_SemGive();

		vTaskDelay(Period * 2);
	}

	return 0;
}
//==============================================================================
