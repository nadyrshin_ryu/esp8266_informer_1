//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef DISPLAY_ANIMATIONS_H_
#define DISPLAY_ANIMATIONS_H_

// �������� ������������ ������ �� �����������
#define HALIGN_LEFT		0
#define HALIGN_RIGHT	1
#define HALIGN_CENTER	2

#define DispWidth DISP1COLOR_Width


// ���, ���������� ��������� �� ������� RunIn ��������
typedef int16_t (*t_ani_runin)(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t halign, uint16_t Period);
// ���, ���������� ��������� �� ������� RunOut ��������
typedef int16_t (*t_ani_runout)(uint8_t *DispBuff, uint8_t *TempBuff, uint16_t BuffLen, uint8_t halign, uint16_t Period);



// ������� ������ ����������� � ������������� (��� ��������)
int16_t ani_Run(uint8_t *NewFrameBuff, uint16_t BuffLen, uint8_t H_Align);
// ������� ��������� ��������� �������� ��������� �������
int16_t ani_RunIn(uint8_t effect, uint8_t *NewFrameBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Speed);
// ������� ��������� ��������� �������� ������������ �������
int16_t ani_RunOut(uint8_t effect, uint8_t *NewFrameBuff, uint16_t BuffLen, uint8_t H_Align, uint16_t Speed);

// ��������� ���������� ���������� �� ������ �����
void ani_UpdateDisplay(void);
// ��������� ������������� ������ ��������
void ani_Init(void);
// ��������� ������� ������� � ���������� � ��� ������ �����
void ani_SemTake();
// ��������� ������������ ������� � ���������� � ��� ������ �����
void ani_SemGive();


#endif /* DISPLAY_ANIMATIONS_H_ */
