//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <freertos/FreeRTOS.h>
#include <freertos/FreeRTOSConfig.h>
#include <disp1color.h>
#include <animations.h>
#include <ani1.h>


//==============================================================================
// �������, ����������� �������� ��������� ������� �� �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani1_RunIn(uint8_t *DispBuff, uint8_t *TempBuff,
		uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t x = DispWidth;
	int16_t newx = GetXposAligned(BuffLen, H_Align);

	// ���������
	for (; x >= newx; x--)
	{
		// ����������� ������ � �������
		ani_SemTake();

		disp1color_DrawBuffer(x, 0, TempBuff, 0, 0, BuffLen, 8);
		disp1color_DrawLine(x + BuffLen, 0, x + BuffLen, 7, 0);
		ani_UpdateDisplay();

		// ����������� ������ � �������
		ani_SemGive();

		vTaskDelay(Period);
	}

	return 0;
}
//==============================================================================


//==============================================================================
// �������, ����������� �������� ������������ ������� � �������
//==============================================================================
int16_t ICACHE_FLASH_ATTR ani1_RunOut(uint8_t *DispBuff, uint8_t *TempBuff,
		uint16_t BuffLen, uint8_t H_Align, uint16_t Period)
{
	int16_t x = GetXposAligned(BuffLen, H_Align);
	int16_t newx = -BuffLen;

	// ������������
	for (; x >= newx; x--)
	{
		// ����������� ������ � �������
		ani_SemTake();

		disp1color_DrawBuffer(x, 0, TempBuff, 0, 0, BuffLen, 8);
		disp1color_DrawLine(x + BuffLen, 0, x + BuffLen, 7, 0);
		ani_UpdateDisplay();

		// ����������� ������ � �������
		ani_SemGive();

		vTaskDelay(Period);
	}

	return 0;
}
//==============================================================================
