//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// ����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "esp_common.h"
#include <freertos/FreeRTOS.h>
#include <freertos/FreeRTOSConfig.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include <freertos/semphr.h>
#include <freertos/timers.h>
#include "../driver/gpio.h"
#include "../display/font.h"
#include "clocktask.h"


// ����� ��� ������ ���� 00:00:00
uint8_t TimeBuff[10];
// ������ ������� ������ � ��������
xQueueHandle xSemTimeBuff;


//------------------------------------------------------------------------------
// ���� ������ ���������� ������� �� �������
//------------------------------------------------------------------------------
void clocktask(void *pvParameters)
{
	while (1)
	{
		// ������ �� RTC �����, �������� ����, ������, �������
		uint32_t S = system_get_rtc_time()/200000;
		uint8_t M = S / 60;
		S %= 60;
		uint8_t H = M / 24;
		M %= 24;

		// ��������� ������, ������� ������ � ��� ��������
		xSemaphoreTake(xSemTimeBuff, 0);
		sprintf(TimeBuff, "%02d:%02d:%02d", H, M, S);
		xSemaphoreGive(xSemTimeBuff);

		// �������� ����� ������������ ������� � ����� ��
		vTaskDelay(10);
	}
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// ��������� ������� ������ ���������� ������ � ������� ��������
//------------------------------------------------------------------------------
void clocktask_start(void)
{
	xSemTimeBuff = xSemaphoreCreateMutex();
	xTaskCreate(clocktask, "ClockTask", 350/*256*/, NULL, 0, NULL);
}
//------------------------------------------------------------------------------
